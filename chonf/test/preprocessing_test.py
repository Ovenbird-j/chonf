# -*- coding: utf-8 -*-
# pylint: disable=duplicate-code
"""Test preprocessor mechanism."""
import json
from functools import partial

import pytest

from chonf import ConfigLoadingIncomplete, Option, Required, load
from chonf.exceptions import InvalidOption


@pytest.fixture(scope="module")
def preset_load(tmp_path_factory):
    """Save json data to a temp path and return a loader."""
    tmp_dir = tmp_path_factory.mktemp("chonf_preprocessing_test")
    data = {
        "a": "1",
        "b": {"a": 1, "b": {"a": 1.5, "b": {"a": "one"}}},
        "c": "1.0",
        "d": "1.5",
        "e": 1.0,
    }
    with open(tmp_dir / "config.json", "w", encoding="utf-8") as json_file:
        json.dump(data, json_file)
    return partial(
        load,
        author="me",
        name="myapp",
        path=tmp_dir,
    )


def test_numeric_success(preset_load):
    """Tests a simple pre-processing function that tries
    to convert certain options to floats, in this case they should
    succeed.
    """

    def into_float(value):
        try:
            return float(value)
        except ValueError:
            raise InvalidOption(value, "convertible to float") from ValueError

    model = {
        "a": Option(preprocess=into_float),
        "b": {
            "a": Option(preprocess=into_float),
            "b": {"a": Required(preprocess=into_float), "b": {"a": Required()}},
        },
    }
    config = preset_load(model=model)
    assert config == {"a": 1.0, "b": {"a": 1.0, "b": {"a": 1.5, "b": {"a": "one"}}}}


def test_numeric_failure(preset_load):
    """Tests a simple pre-processing function that tries
    to convert ceretain options to integers, in this case some
    should fail and some should succeed.
    """

    def into_int(value):
        # test if number can be read as integer, even if it has a dot
        try:
            fvalue = float(value)
            ivalue = int(fvalue)
            if fvalue == ivalue:
                return ivalue
            raise InvalidOption(value, "convertible to int")
        except ValueError as err:
            raise InvalidOption(value, "convertible to int") from err

    model = {
        "a": Option(preprocess=into_int),
        "b": {
            "a": Option(preprocess=into_int),
            "b": {
                "a": Required(preprocess=into_int),
                "b": {
                    "a": Required(preprocess=into_int),
                },
            },
        },
        "c": Option(preprocess=into_int),
        "d": Option(preprocess=into_int),
        "e": Option(preprocess=into_int),
    }
    with pytest.raises(ConfigLoadingIncomplete) as err:
        preset_load(model=model)
    assert err.value.loaded_configs == {
        "a": 1,
        "b": {
            "a": 1,
            "b": {
                "a": InvalidOption(1.5, "convertible to int"),
                "b": {
                    "a": InvalidOption("one", "convertible to int"),
                },
            },
        },
        "c": 1,
        "d": InvalidOption("1.5", "convertible to int"),
        "e": 1,
    }
