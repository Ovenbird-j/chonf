"""Provides access to python scripts as config files.

Chonf allows users to define python config files named "config.py"
in the configuration directory. The configs can be defined either
in a nested dic (or any nested map that fits) or in a callable that
receives no arguments and returns such dict.

Example with dictionary:

>>> configs = {
>>>     "option1": "value1",
>>>     "section1": {
>>>         "option2": "value2",
>>>         "repeat": {
>>>             "one": 1,
>>>             "two": 2,
>>>             "three": 3,
>>>         },
>>>     },
>>>     "empty": { },
>>> }

Equivalent example with function:

>>> def configs():
>>>     return {
>>>         "option1": "value1",
>>>         "section1": {
>>>             "option2": "value2",
>>>             "repeat": {
>>>                 "one": 1,
>>>                 "two": 2,
>>>                 "three": 3,
>>>             },
>>>         },
>>>         "empty": { },
>>>     }

The function version allows users to change configs dynamically
in any sort of way without adding extra work to the developers of
the actual program. This feature is intended for expert users that
might want to dynamically change things depending on their own env
variables, virtual envs, etc.

"""
from chonf.parsers.python.core import list_children, read  # noqa
