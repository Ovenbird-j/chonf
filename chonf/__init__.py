# -*- coding: utf-8 -*-
"""Utilities for accessing user configurations from multiple posible sources.

Functions:

    * load - Tries to load user configurations from multiple sources
    * default_path - Generates list of default config file locations, OS aware

"""
from chonf.core import Option, Repeat, Required, load
from chonf.exceptions import ConfigLoadingIncomplete, ConfigReadingError, InvalidOption
from chonf.paths import default_path
